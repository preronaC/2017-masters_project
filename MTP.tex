\documentclass[xcolor=svgnames]{beamer}
\usepackage[utf8]    {inputenc}
\usepackage[T1]      {fontenc}
\usepackage[english] {babel}
\input{macros}
\usepackage{MnSymbol}
\usepackage{amsmath,amssymb,amsfonts,graphicx}
\usepackage{beamerleanprogress}
\usepackage[linesnumbered,vlined,boxed,ruled]{algorithm2e}
%\usepackage{MathUnicode}
\usepackage{commons}
\usepackage{tikz}
\usetikzlibrary{matrix}
\usetikzlibrary{calc}
\newcommand{\tmark}[1]{\tikz[remember picture, overlay] \node(#1){};}
\renewcommand{\phi}{\varphi}

\title{Towards Algebraic Independence based PITs over Arbitrary fields}
\author
  {Prerona Chatterjee}

\date{December 08, 2017}
\institute{TIFR, Mumbai}

\begin{document}
\maketitle

\section{Introduction}

\begin{frame}{A little about Algebraic Independence}
    \begin{block}{Definition: Algebraic Independence}
    A given set of polynomials $\set{\fm} \subseteq \F[\x]$ is said to be algebraically dependent if there is a non-zero polynomial combination of these that is zero.\\ \\    
    Otherwise, they are said to be algebraically independent.
    \end{block}\pause
	
	\begin{itemize}
		\item For a set of polynomials $\set{\fm}$, the family of all algebraically independent subsets form a matroid. Thus,
		\vspace{-.5em}
		\[\algrank(\fm) \text{ is well defined.}\] \pause
		\vspace{-1em}
		\item \cite{Kay09} The minimal "annihilating polynomial" is "hard".
	\end{itemize}
\end{frame}

\section{Characteristic Zero Fields}

\begin{frame}{Checking Algebraic Independence efficiently}
	For $\fm \in \F[\x]$ and $\vecf = (\fm)$, 
	\[\J_\vecx(\vecf) = 
	\begin{bmatrix} %change order
	\partial_{x_1}(f_1) & \partial_{x_2}(f_1) & \ldots & \partial_{x_n}(f_1)\\
	\partial_{x_1}(f_2) & \partial_{x_2}(f_2) & \ldots & \partial_{x_n}(f_2)\\
	\vdots & \vdots & \ddots & \vdots\\
	\partial_{x_1}(f_m) & \partial_{x_2}(f_m) & \ldots & \partial_{x_n}(f_m)\\
	\end{bmatrix}\] \pause
    \begin{block}{The Jacobian Criterion}
    If $\F$ has characteristic zero, $\set{\fm}$ is algebraically independent if and only if its Jacobian matrix is full rank.
    \end{block} 
\end{frame}

\begin{frame}{How it helps in solving PITs}
\begin{block}{Definition: Faithful Maps}
	Given a set of polynomials $\set{\fm}$ with algebraic rank $k$, a map 
	\vspace{-1em}
	\[\phi: \set{\x} \to \F(\yk)\] is said to be a faithful map if the algebraic rank of $\set{f_1(\phi), f_2(\phi), \ldots, f_m(\phi)}$ is also $k$.
\end{block}\pause
\textbf{The PIT Question}: Given a circuit $\ckt$, check whether it computes the identically zero polynomial.\\ \pause
\vspace{.5em}
\textbf{The Connection} \cite{BMS11, ASSS12}: Given a set of polynomials $\set{\fm}$ and a faithful map $\phi$; for any circuit $\ckt(z_1, \ldots, z_m)$,
\vspace{-.5em} \[\ckt(\fm) \neq 0 \Leftrightarrow \inparen{\ckt(f_1(\phi), f_2(\phi), \ldots f_m(\phi))} \neq 0.\]
\end{frame}

\begin{frame}{The Strategy}
	\vspace{-2em}
	\[\phi : x_i = \sum_{j=1}^{k}s_{ij}y_j + a_i\] \pause
	\vspace{.5em}
	\only<2-2>{\[\begin{bmatrix}
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \J_\vecy(\vecf(\phi)) & \text{ }\\ 
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \text{ } & \text{ }\\
		\end{bmatrix}\]}
	\only<3->{\[\begin{bmatrix}
	\text{ } & \text{ } & \text{ }\\
	\text{ } & \text{ } & \text{ }\\
	\text{ } & \J_\vecy(\vecf(\phi)) & \text{ }\\ 
	\text{ } & \text{ } & \text{ }\\
	\text{ } & \text{ } & \text{ }\\
	\end{bmatrix}
	=
	\begin{bmatrix}
	& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\ 
	& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \J_\vecx(\vecf)|_\phi & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &
	\end{bmatrix}
	\times 
	\begin{bmatrix}
	& \text{ } & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } &\\
	& \text{ } & M_\phi & \text{ } &\\ 
	& \text{ } & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } &\\
	\end{bmatrix}\]}
	\vspace{.5em}
	\only<4->{\textbf{What we need:} $\phi$ such that
	\begin{enumerate}
		\item $\rank(\J_\vecx(\vecf)) = \rank(\J_\vecx(\vecf)|_\phi)$
		\only<5-5>{: Can be handled by choosing $a_i$s\\
		\hspace*{12.5em} correctly.}
		\only<6->{\item $\rank(\J_\vecx(\vecf)|_\phi) = \rank(\J_\vecx(\vecf)|_\phi \times M_\phi)$}
	\end{enumerate}}
\end{frame}

\begin{frame}{Rank Extractors}
	\begin{block}{Definition: Rank Extractors}
		An $n$-rowed matrix $M$ is said to be a rank extractor if for every $m \times n$ matrix $A$, $\rank(A) = \rank(AM)$.
	\end{block} \pause
	\vspace{.5em}
	\only<2-2>{\[\begin{bmatrix}
	& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\ 
	& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & A & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &
	\end{bmatrix}_{m \times n}\]}
	\only<3-3>{\[\begin{bmatrix}
	& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\ 
	& \text{ } & \text{ } & A' & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &
	\end{bmatrix}_{k \times n}\]}
\only<4->{\[\begin{bmatrix}
	& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\ 
	& \text{ } & \text{ } & A' & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &
	\end{bmatrix}
	\times 
	\begin{bmatrix}
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \text{ } & \text{ }\\
		\text{ } & M_s & \text{ }\\ 
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \text{ } & \text{ }
	\end{bmatrix}
	=
	\begin{bmatrix}
	& \text{ } &\\
	& A'M_s &\\ 
	& \text{ } &
	\end{bmatrix}\]}
\end{frame}

\begin{frame}{A Faithful map}
\begin{columns}
	\begin{column}{.35\textwidth}
	\only<1-4>{\[\begin{array}{r}
		x_1\\
		x_2\\
		\vdots\\
		\vdots\\
		\vdots\\
		\vdots\\
		x_n
		\end{array}
		\begin{bmatrix}
		& \text{ } & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \text{ } &\\
		& \text{ } & M & \text{ } &\\ 
		& \text{ } & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \text{ } &
		\end{bmatrix}\]}
	\only<5-5>{\[\begin{array}{r}
		x_1\\
		x_2\\
		\vdots\\
		\vdots\\
		\vdots\\
		\vdots\\
		x_n
		\end{array}
		\begin{bmatrix}
		s^{\wt(1)} & \hspace{4em}\\
		s^{\wt(2)} & \hspace{4em}\\
		\vdots & \hspace{4em}\\
		\vdots & \hspace{4em}\\
		\vdots & \hspace{4em}\\
		\vdots & \hspace{4em}\\
		s^{\wt(n)} & \hspace{4em}
		\end{bmatrix}\]}
	\only<6-6>{\[\hspace{-.5em}\begin{bmatrix}
		\inparen{s^{\wt(1)}}^1 & \ldots & (s^{\wt(1)})^k\\
		\inparen{s^{\wt(2)}}^1& \ldots & \inparen{s^{\wt(2)}}^k\\
		\vdots & \text{ } & \vdots\\
		\vdots & \ddots & \vdots\\
		\vdots & \ddots & \vdots\\
		\vdots & \text{ } & \vdots\\
		\inparen{s^{\wt(n)}}^1 & \ldots & \inparen{s^{\wt(n)}}^k
		\end{bmatrix}\]}
	\only<7->{\newline
		\cite{GR05}: Vandermonde type matrices are rank extractors.
		\vspace{.5em}
		\[\begin{bmatrix}
			s & \ldots & s^k\\
			\inparen{s^2}^1& \ldots & \inparen{s^2}^k\\
			\vdots & \text{ } & \vdots\\
			\vdots & \ddots & \vdots\\
			\vdots & \ddots & \vdots\\
			\vdots & \text{ } & \vdots\\
			\inparen{s^n}^1 & \ldots & \inparen{s^n}^k
			\end{bmatrix}\]}
	\end{column}\pause
	\begin{column}{.65\textwidth}
		Binet-Cauchy:
		\vspace{-.5em}
		\[\det(AM) = \sum_{B \subseteq \set{x_i} \text{, } \abs{B} = k} \det(A_B) \det(M_B).\] \pause
		
		\vspace{.5em}
		\textbf{Sufficient Properties}
		\begin{enumerate}
		\item Every $k \times k$ minor is full rank.\pause
		\item From among the $B$s for which $\det(A_B) \neq 0$, there is a unique $B$ for which the $\deg_s(\det(M_B))$ is maximum.
		\end{enumerate}
	\end{column}
\end{columns}
\vspace{1em}
\only<5-7>{\begin{itemize}
		\item Define $\wt(x_i)$ such that the weight of each row is distinct.
		\item Extend definition to minors cleverly: $\wt(B) = \deg_s(\det(M_B))$.
		\end{itemize}}
\only<8-> {\[\phi : x_i = \sum_{j=1}^{k}s^{ij}y_j + a_i \text{ will work.}\]}
\end{frame}

\section{Arbitrary Fields}

\begin{frame}{Failure of the Jacobian Criterion over Arbitrary fields}
	$f_1 = xy^{p-1}$, $f_2 = x^{p-1}y$ : Algebraically Independent over $\F_p$.\pause
	\vspace{.5em}
	\[\J_{x,y} = \begin{bmatrix}
		y^{p-1} & (p-1)xy^{p-2}\\
		(p-1)x^{p-2}y & x^{p-1}
		\end{bmatrix}\]
		\[\det(\J_{x,y}) = (xy)^{p-1} - (p^2 - 2p + 1) (xy)^{p-1} = 0 \text{ over }\F_p.\]\pause
		
	\textbf{Reason:} $(x,f_1,f_2): A_x(\alpha, \beta, \gamma)$ \quad \quad \pause 
	$(y,f_1, f_2): A_y(\alpha, \beta, \gamma)$ \pause
	\[\partial_\alpha(A_x) = 0 = \partial_\alpha(A_y)\] \pause
	\vspace{-1.5em}
	\[A_x(\alpha, \beta, \gamma) = A'_x(\alpha^{p^{k_1}}, \beta, \gamma), A_y(\alpha, \beta, \gamma) = A'_y(\alpha^{p^{k_2}}, \beta, \gamma)\]\pause
	
	\vspace{.5em} For $k = \max\set{k_1, k_2}$, $p^k: $ Inseparable degree of $\set{f_1, f_2}$.
\end{frame}

\begin{frame}{Hasse derivatives}
	For any $f \in \F[\x]$ and $\vecz \in \F^n$,
	\[f(\vecx + \vecz) - f(\vecz) = \underbrace{x_1 \cdot \partial_{x_1}f + \cdots + x_n \cdot \partial_{x_n}f}_{\text{Jacobian}} + \text{ higher order terms}\]\pause
	
	For $f = x^p$, $f(x + z) - f(z) = x^p$ over $\F_p$. \pause
	
	\vspace{1em}
	\textbf{Consider Hasse Derivatives: }
	\[\partial^h_{x^p}(x^p) = \frac{1}{p!} \times p! = 1\]\pause
	In general, the Hasse derivative of $f$ with respect to $\vecx^\vece$ is the coefficient of $\vecx^\vece$ in $f(\vecx + \vecz) - f(\vecz)$.
\end{frame}

\begin{frame}{The Criterion over Arbitrary fields}
	\begin{columns}
		\begin{column}{.55\textwidth}
			\begin{block}{Definition: A new Operator}
				For any $f \in \F[\x]$,
				\[\Ech_t(f) = \deg^{\leq t} \inparen{f(\vecx + \vecz) - f(\vecz)}\]
			\end{block}
		\end{column}\pause
		\begin{column}{.45\textwidth}
			\[\hat{\Ech}(\vecf) = \begin{bmatrix}
			& \ldots & \Ech_t(f_1) & \ldots & \\
			& \ldots & \Ech_t(f_2) & \ldots & \\
			& & \vdots\\
			& \ldots & \Ech_t(f_m) & \ldots &
			\end{bmatrix}.\]
		\end{column}\pause
	\end{columns}
	\begin{block}{The \cite{PSS16} Criterion}
		A given set of polynomials $\set{\fm} \in \F[\x]$ is algebraically independent if and only if for a random $\vecz \in \F^n$, $\set{\Ech_t(f_1), \Ech_t(f_2), \ldots, \Ech_t(f_m)}$ are linearly independent in  \[\frac{\F(\vecz)[\x]}{\I_t}\] where $t$ is the inseparable degree of $\set{\fm}$ and $\I_t$ is some fixed ideal of $\F(\vecz)[\x]$.
	\end{block} 
\end{frame}

\begin{frame}{Alternate Statement for the \cite{PSS16} criterion}

	$\set{\fm}$ is algebraically independent if and only if for every $\inparen{v_1, v_2, \ldots, v_k}$ with $v_i \text{s in } \I_t$,
	\[\Ech(\vecf, \vecv) = \begin{bmatrix}
	& \ldots & \Ech_t(f_1) + v_1 & \ldots & \\
	& \ldots & \Ech_t(f_2) + v_2 & \ldots & \\
	& & \vdots\\
	& \ldots & \Ech_t(f_k) + v_k & \ldots &
	\end{bmatrix} \text{ has full rank over } \F(\vecz).\]
\end{frame}

\begin{frame}{What we want to show}
	\[\Ech(\vecf(\phi), \vecu) = \begin{bmatrix}
	& \ldots & \Ech_t(f_1(\phi)) + u_1 & \ldots & \\
	& \ldots & \Ech_t(f_2(\phi)) + u_2 & \ldots & \\
	& & \vdots\\
	& \ldots & \Ech_t(f_m(\phi)) + u_m & \ldots &
	\end{bmatrix}\] 
	\begin{center}
		has full rank for every $u_1, u_2, \ldots, u_k \in \I_t(\phi)$ whenever 
	\end{center}\pause
	\[\Ech(\vecf, \vecv) = \begin{bmatrix}
	& \ldots & \Ech_t(f_1) + v_1 & \ldots & \\
	& \ldots & \Ech_t(f_2) + v_2 & \ldots & \\
	& & \vdots\\
	& \ldots & \Ech_t(f_k) + v_k & \ldots &
	\end{bmatrix}\] 
	\begin{center}
		has full rank for every $v_1, v_2, \ldots, v_k \in \I_t$.
	\end{center}
\end{frame}

\begin{frame}{The Strategy}
	\vspace{-2em}
	\only<1-8>{\[\phi : x_i \to \sum_{j=1}^{k}s_{ij}y_j + a_i \text{ and } z_i \to \sum_{j=1}^{k}s_{ij}w_j + a_i\] \pause}
	\only<9-9>{\[\phi : x_i \to \sum_{j=1}^{k}{\color{blue}s^{ij}}y_j + a_i \text{ and } z_i \to \sum_{j=1}^{k}{\color{blue}s^{ij}}w_j + a_i\]}
	\only<10-13>{\[\phi : x_i \to \sum_{j=1}^{k}\alert<10-10>{s^{j(t+1)^i}}y_j + a_i \text{ and } z_i \to \sum_{j=1}^{k}\alert<10-10>{s^{j(t+1)^i}}w_j + a_i\]}
	\only<14->{\[\phi : x_i \to \sum_{j=1}^{k}s^{j(t+1)^i}y_j + a_i \text{ and } z_i \to \sum_{j=1}^{k}s^{j(t+1)^i}w_j + a_i.\] where $t$ is the inseparable degree.\\}
	\vspace{.5em}
	\only<1-10>{\underline{\textbf{Sufficient Properties}}}
	\only<11->{\underline{\textbf{Properties}}\\}
	\begin{enumerate}
		\only<1-6>{\item \textbf{Every $u$ must have a $v$}\pause : There is a natural pre-image. \pause}
		\only<7->{\item $\Ech(\vecf(\phi), \vecu) = \Ech(\vecf(\phi), \vecv(\phi))$ for some appropriate $\vecv$.}
		\only<4-6>{\item $\Ech(\vecf(\phi), \vecv(\phi)) = \Ech(\vecf,\vecv)|_{\phi} \times M_\phi$} \only<5-6>{: True in general.}
		\only<7->{\item $\Ech(\vecf,\vecv)|_{\phi} \times M_\phi$ is a sub-matrix of $\Ech(\vecf(\phi), \vecv(\phi))$}
		\only<6-6>{\[ \hspace{-2em} \begin{bmatrix}
			& \text{ } & \text{ } & \text{ } &\\ 
			& \text{ } & \text{ } & \text{ } &\\
			& \text{ } & \Ech(\vecf(\phi), \vecv(\phi)) & \text{ } &\\
			& \text{ } & \text{ } & \text{ } &\\
			& \text{ } & \text{ } & \text{ } &
			\end{bmatrix}
			= 
			\begin{bmatrix}
			& \text{ } & \text{ } & \text{ }\\
			& \text{ } & \text{ } & \text{ }\\
			& \text{ } & \text{ } & \text{ }\\
			& \text{ } & \Ech(\vecf, \vecv)|_\phi & \text{ }\\ 
			& \text{ } & \text{ } & \text{ }\\
			& \text{ } & \text{ } & \text{ }\\
			& \text{ } & \text{ } & \text{ }\\
			\end{bmatrix}
			\times
			\begin{bmatrix}
			& \text{ } & \text{ } & \text{ } &\\
			& \text{ } & \text{ } & \text{ } &\\
			& \text{ } & M_{\phi} & \text{ } &\\ 
			& \text{ } & \text{ } & \text{ } &\\
			& \text{ } & \text{ } & \text{ } &\\
			\end{bmatrix}\]}
		\only<7->{\item $\rank(\Ech(\vecf, \vecv)|_\phi) = \rank(\Ech(\vecf,\vecv)|_\phi \times M_\phi)$}
		\only<8-8>{: \[\underbrace{\begin{bmatrix}
				& \text{ } & \text{ } & \text{ } &\\
				& \text{ } & \text{ } & \text{ } &\\
				& \text{ } & M_{\phi} & \text{ } &\\ 
				& \text{ } & \text{ } & \text{ } &\\
				& \text{ } & \text{ } & \text{ } &
				\end{bmatrix}}_{\text{labelled by monomials of degree up to } t \text{ in } \vecy}\]}
		\only<9-9>{: {\color{blue} $\wt(x_i) = i$} \[\underbrace{\begin{bmatrix}
				& \text{ } & \text{ } & \text{ } &\\
				& \text{ } & \text{ } & \text{ } &\\
				& \text{ } & M_{\phi} & \text{ } &\\ 
				& \text{ } & \text{ } & \text{ } &\\
				& \text{ } & \text{ } & \text{ } &
				\end{bmatrix}}_{\text{labelled by monomials of degree up to } t \text{ in } \vecy} \text{Not Block Vandermonde type}\]}
		\only<10-10>{
		\hspace{-.65em}: \alert<10-10>{$\wt(x_i) = (t+1)^i$}
		\[\underbrace{\begin{bmatrix}
					\text{ } & \text{ } & \text{ }\\
					\text{ } & \text{ } & \text{ }\\
					\text{ } & M_{\phi} & \text{ }\\ 
					\text{ } & \text{ } & \text{ }\\
					\text{ } & \text{ } & \text{ }
					\end{bmatrix}}_{\text{labelled by \alert<10-10>{"pure"} monomials of degree up to } t \text{ in } \vecy} \text{Block Vandermonde type}\]}
		\only<11-11>{
		\hspace{-1em}: $\wt(x_i) = (t+1)^i$ \[\overbrace{\begin{bmatrix}
				& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\ 
				& \text{ } & \text{ } & A' & \text{ } & \text{ } &\\
				& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &
			\end{bmatrix}}^{\text{ labelled by }\vecx^\vece}
			\times 
			\begin{bmatrix}
				& \text{ } & \text{ } & \text{ } &\\
				& \text{ } & \text{ } & \text{ } &\\
				& \text{ } & \text{ } & \text{ } &\\
				& \text{ } & M_\phi & \text{ } &\\ 
				& \text{ } & \text{ } & \text{ } &\\
				& \text{ } & \text{ } & \text{ } &\\
				& \text{ } & \text{ } & \text{ } &
			\end{bmatrix}
			=
			\begin{bmatrix}
				\text{ } & \text{ } & \text{ } &\\
				\text{ } & A'M_\phi & \text{ } &\\ 
				\text{ } & \text{ } & \text{ } &
		\end{bmatrix}\]}
	\only<12-12>{
		\hspace{-1.325em}: $\wt(x_i) = (t+1)^i$ \[\begin{bmatrix}
		& \text{ } & \tmark{a}\text{ } & \text{ } & \text{ } & \text{ } &\\ 
		& \text{ } & \text{ } & A' & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \text{ } & \text{ }\tmark{b} & \text{ } &
		\end{bmatrix}
		\times 
		\begin{bmatrix}
		& \text{ } & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \text{ } &\\
		\tmark{c} & \text{ } & \text{ } & \text{ } &\\
		& \text{ } & M_\phi & \text{ } &\\ 
		& \text{ } & \text{ } & \text{ } &\tmark{d}\\
		& \text{ } & \text{ } & \text{ } &\\
		& \text{ } & \text{ } & \text{ } &
		\end{bmatrix}
		=
		\begin{bmatrix}
		\text{ } & \text{ } & \text{ } &\\
		\text{ } & A'M_\phi & \text{ } &\\ 
		\text{ } & \text{ } & \text{ } &
		\end{bmatrix}\]
		\begin{tikzpicture}[remember picture, overlay]
		\node (t) at ($(a)+(.5cm, 1cm)$) {\text{\small{unique minor with max. wt.}}};
		\draw[blue, thick] ($(a)+(.05cm,.1cm)$) rectangle ($(b)-(.05cm,0cm)$); 
		\draw[blue, thick] ($(c)+(0cm,.1cm)$) rectangle (d);
		\draw[->, blue, thick] ($(a)+(.5cm,.2cm)$) -- (t);
		\end{tikzpicture}}
		\only<13-13>{
		\hspace{-1.65em}: $\wt(x_i) = (t+1)^i$ \[\begin{bmatrix}
			& \text{ } & \tmark{a}\text{ } & \text{ } & \text{ } & \text{ } &\\ 
			& \text{ } & \text{ } & A' & \text{ } & \text{ } &\\
			& \text{ } & \text{ } & \text{ } & \text{ }\tmark{b} & \text{ } &
			\end{bmatrix}
			\times 
			\begin{bmatrix}
			\text{ } & \text{ } & \text{ }\\
			\text{ } & \text{ } & \text{ }\\
			\tmark{c}\text{ } & \text{ } & \text{ }\\
			\text{ } & M_\phi & \text{ }\\ 
			\text{ } & \text{ } & \text{ }\tmark{d}\\
			\text{ } & \text{ } & \text{ }\\
			\text{ } & \text{ } & \text{ }
			\end{bmatrix}
			=
			\begin{bmatrix}
			& \text{ } &\\
			& A'M_\phi &\\ 
			& \text{ } &
			\end{bmatrix}\]
		\begin{tikzpicture}[remember picture, overlay]
		\node (t) at ($(a)+(.5cm, 1cm)$) {\text{\small{unique minor with max. wt.}}};
		\node (s) at ($(c)+(.75cm, -2.5cm)$) {$\small{wt = \deg_s}$};
		\draw[blue, thick] ($(a)+(.05cm,.1cm)$) rectangle ($(b)-(.05cm,0cm)$); 
		\draw[blue, thick] ($(c)+(0cm,.1cm)$) rectangle (d);
		\draw[->, blue, thick] ($(d)+(-.75cm, -.2cm)$) -- (s);
		\draw[->, blue, thick] ($(a)+(.5cm,.2cm)$) -- (t);
		\end{tikzpicture}}
		\only<14->{\item $\rank(\Ech(\vecf, \vecv)) = \rank(\Ech(\vecf,\vecv)|_\phi)$}
		\only<15-> {: Can be handled by choosing the $a_i$s correctly}
	\end{enumerate}
\end{frame}

\begin{frame}{The Map}
\vspace{-1.5em}
	\[\phi : x_i \to \sum_{j=1}^{k}s^{j(t+1)^i \alert<1-1>{\bmod p}}y_j + a_i \text{ and } z_i \to \sum_{j=1}^{k}s^{j(t+1)^i \alert<1-1>{\bmod p}}w_j + a_i.\] where $t$ is the inseparable degree.\pause \\
	\vspace{1.5em}
	\underline{\textbf{Properties}}
	\vspace{.5em}
	\begin{enumerate}
		\item $\Ech(\vecf(\phi), \vecu) = \Ech(\vecf(\phi), \vecv(\phi))$ for some appropriate $\vecv$.
		\item $\Ech(\vecf,\vecv)|_{\phi} \times M_\phi$ is a sub-matrix of $\Ech(\vecf(\phi), \vecv(\phi))$.
		\item $\rank(\Ech(\vecf, \vecv)|_\phi) = \rank(\Ech(\vecf,\vecv)|_\phi \times M_\phi)$.
		\item $\rank(\Ech(\vecf, \vecv)) = \rank(\Ech(\vecf,\vecv)|_\phi)$.
	\end{enumerate}\pause
	\vspace{1em}
	\underline{\textbf{Size bounds}}:  $p = O(n^{3t})$, $s = O(p)$.\pause\\
	\vspace{.5em}
	\underline{\textbf{Choice of $\veca$}}: Depends on the model under consideration.
\end{frame}

\begin{frame}{An Application}
	\begin{block}{Theorem: Extension of \cite{BMS11}}
		If $\set{\fm} \in \F[\x]$ is a set of sparse polynomials with transcendence degree $k$ and inseparable degree $t$, then there is a $n^{\poly(k,t)}$ time $\PIT$ for circuits of the type $\ckt(\fm)$.\\
		
		\noindent Thus if $k$, $t$ were constant, we have a $\poly(n)$-time $\PIT$.
	\end{block}\pause
	\vspace{2em}
	\begin{center}
		\textbf{\large{Thank you!}}
	\end{center}
\end{frame}

\begin{frame}[allowframebreaks]{References}
  \bibliographystyle{alpha}
  \bibliography{bib}
\end{frame}
\end{document}